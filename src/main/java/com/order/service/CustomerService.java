package com.order.service;

import java.util.function.Consumer;

import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.order.Entity.Customer;
import com.order.Entity.CustomerOrder;
import com.order.Entity.TotalAmount;
import com.order.Repository.CustomerRepository;

@Service
public class CustomerService {
	
	
	
	@Autowired
	KieContainer kieContainer;
	
	@Autowired
	private CustomerRepository customerRepository;
	
	private Double basePrice = 0.0d;
	private Double finalPrice = 0.0d;
	
	public void saveOrder(Customer customer) {
		customerRepository.save(customer);
	}
	
	public void applyRulesForCustomer(Customer customer) {
		basePrice = 0.0d;
		finalPrice = 0.0d;
		customer.getOrders().stream().forEach(action);
		
		TotalAmount totalAmount = new TotalAmount();
		
		totalAmount.setItemAmount(finalPrice);
		totalAmount.setShippingCharge(basePrice < 1000 ? 100.0 : 0.0);
		totalAmount.setOrderTotal(finalPrice + totalAmount.getShippingCharge());
		customer.setTotalAmount(totalAmount);
		saveOrder(customer);
		
	}
	
	public CustomerOrder applyDiscount(CustomerOrder customerOrder) {
		KieSession kieSession = kieContainer.newKieSession("rulesSession");
		kieSession.insert(customerOrder);
		kieSession.fireAllRules();
		kieSession.dispose();
		
		return customerOrder;
	}
	
	
	private void calculateDiscount(CustomerOrder customerOrder) {
		int totalDiscountPer = customerOrder.getWeekendDiscount()+customerOrder.getTypeDiscount();
		customerOrder.setItemDiscount((customerOrder.getItemPrice()*totalDiscountPer)/100);
		customerOrder.setItemTotalAmount(customerOrder.getItemPrice()-customerOrder.getItemDiscount());
			
		}
	
	Consumer<CustomerOrder> action = order ->
    {
    	applyDiscount(order);
		calculateDiscount(order);
		basePrice = basePrice + order.getItemPrice();
		finalPrice = finalPrice + order.getItemTotalAmount();
    };
	


}
