package com.order.Entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
@Entity
@Table
public class TotalAmount {
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	@JsonIgnore
	private Long totalAmountId;
	@Column
	@JsonProperty("item-amount")
	private Double itemAmount;
	@Column
	@JsonProperty("shipping-charge")
	private Double shippingCharge;
	@Column
	@JsonProperty("order-total")
	private Double orderTotal;
	public Double getItemAmount() {
		return itemAmount;
	}
	public void setItemAmount(Double itemAmount) {
		this.itemAmount = itemAmount;
	}
	public Double getShippingCharge() {
		return shippingCharge;
	}
	public void setShippingCharge(Double shippingCharge) {
		this.shippingCharge = shippingCharge;
	}
	public Double getOrderTotal() {
		return orderTotal;
	}
	public void setOrderTotal(Double orderTotal) {
		this.orderTotal = orderTotal;
	}
	public Long getTotalAmountId() {
		return totalAmountId;
	}
	public void setTotalAmountId(Long totalAmountId) {
		this.totalAmountId = totalAmountId;
	}

}
